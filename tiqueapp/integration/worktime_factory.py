
from tiqueapp.integration.worktime_rescuetime import WorktimeRescuetime

class WorktimeFactory(object):

    @staticmethod
    def instance(data):
        if data['vendor'] == 'rescuetime':
            return WorktimeRescuetime(data)
