import colorlog
import requests

from string import Template

class WorktimeRescuetime(object):

    def __init__(self, data):
        handler = colorlog.StreamHandler()
        handler.setFormatter(colorlog.ColoredFormatter('%(log_color)s%(name)s - %(message)s'))
        self.logger = colorlog.getLogger("WorktimeRescuetime")
        self.logger.addHandler(handler)
        self.logger.setLevel(20)
        self.data = data
        self.template = Template('https://www.rescuetime.com/anapi/data?key=$KEY&perspective=interval&interval=day&restrict_begin=$DATE_BEGIN&restrict_end=$DATE_END&format=json&restrict_kind=productivity')

    def get_seconds_by_date(self, date):
        req = requests.get(self.template.substitute(KEY=self.data['apikey'],DATE_BEGIN=date,DATE_END=date))
        if req.status_code == 200:
            items = req.json()
            total_worktime = 0
            for row in items['rows']:
                if row[3] >= 1:
                    total_worktime += row[1]
            return total_worktime
        else:
            self.logger.error(self.template.substitute(KEY=self.data['apikey'],DATE_BEGIN=date,DATE_END=date))
            self.logger.error(req)
            self.logger.error(req.text)
            return None
