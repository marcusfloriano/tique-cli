
from tiqueapp.client_bitbucket import ClientBitbucket

class ClientFactory(object):

    @staticmethod
    def instance(data):
        if data['vendor'] == 'bitbucket':
            return ClientBitbucket(data)
