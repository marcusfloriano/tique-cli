# -*- coding: utf-8 -*-

import hashlib
import json

from tinydb import TinyDB, Query
from terminaltables import AsciiTable

from colorama import init
from termcolor import colored

from tiqueapp.base import Base
from tiqueapp.dao.worktime_dao import WorktimeDAO
from tiqueapp.client_factory import ClientFactory

class Worktime(Base):

    intro = colored('Worktime.   Type help or ? to list commands.\n', 'green')
    prompt = colored('(tique)', 'green') + colored('[worktime] ', 'yellow')

    def __init__(self):
        Base.__init__(self, 'Worktime');

    def do_remove(self, args):
        args = self.parse(args)
        if len(args) == 0:
            self.logger.error('Sorry, not passed key for remove!')
        else:
            if WorktimeDAO().remove(args[0]):
                print(colored('OK, removed with success!', 'green'))
            else:
                print(colored('Sorry, not removed!', 'red'))
