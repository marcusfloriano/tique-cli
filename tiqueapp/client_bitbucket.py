import colorlog

import datetime
from datetime import timedelta, date

from pybitbucket.bitbucket import Client, BadRequestError, ServerError
from pybitbucket.ref import Tag
from pybitbucket.commit import Commit
from pybitbucket.auth import (BasicAuthenticator)

class ClientBitbucket(object):

    def __init__(self, data):
        handler = colorlog.StreamHandler()
        handler.setFormatter(colorlog.ColoredFormatter('%(log_color)s%(name)s - %(message)s'))
        self.logger = colorlog.getLogger("ClientBitbucket")
        self.logger.addHandler(handler)
        self.logger.setLevel(20)

        self.data = data
        self.client = Client(
            BasicAuthenticator(
                self.data['username'],
                self.data['password'],
                self.data['email']
            )
        )

    def tags(self):
        items = Tag.find_tags_in_repository(
            self.data['repository'],
            self.data['owner'],
            client=self.client
        )
        result = []
        for item in items:
            result.append({
                'name' : item.name,
                'target' : item.target.hash
            })
        return result

    def commits_between_tags(self, tag1, tag2):

        commit1 = Commit.find_commit_in_repository_by_revision(
            username=self.data['owner'],
            repository_name=self.data['repository'],
            revision=tag1,
            client=self.client
        )
        commit1_date = datetime.datetime.strptime(commit1.date, '%Y-%m-%dT%H:%M:%S+00:00')

        commit2 = Commit.find_commit_in_repository_by_revision(
            username=self.data['owner'],
            repository_name=self.data['repository'],
            revision=tag2,
            client=self.client
        )
        commit2_date = datetime.datetime.strptime(commit2.date, '%Y-%m-%dT%H:%M:%S+00:00')

        commit_start = None
        commit_end = None
        tag = None
        if commit1_date < commit2_date:
            commit_start = commit2
            commit_end = commit1
            tag = tag2
        elif commit2_date < commit1_date:
            commit_start = commit1
            commit_end = commit2
            tag = tag1

        commits = Commit.find_commits_in_repository(
            username=self.data['owner'],
            repository_name=self.data['repository'],
            branch=tag,
            client=self.client
        )

        result = []
        tags = self.tags()
        tag_name = None
        for commit in commits:
            if(commit.hash == commit_end.hash):
                break;
            for tag in tags:
                if(commit.hash == tag['target']):
                    tag_name = tag['name']
            result.append({
                'tag' : tag_name,
                'hash' : commit.hash[0:7],
                'message' : commit.message,
                'date' : datetime.datetime.strptime(commit.date, '%Y-%m-%dT%H:%M:%S+00:00').strftime('%d/%m/%Y')
            })

        return result
