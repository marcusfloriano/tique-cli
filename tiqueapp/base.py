# -*- coding: utf-8 -*-

import colorlog
import cmd, sys

from tinydb import TinyDB, Query

from jinja2 import Environment, PackageLoader

class Base(cmd.Cmd):

    def __init__(self, loggername):
        cmd.Cmd.__init__(self)
        handler = colorlog.StreamHandler()
        handler.setFormatter(colorlog.ColoredFormatter('%(log_color)s%(name)s - %(message)s'))
        self.logger = colorlog.getLogger(loggername)
        self.logger.addHandler(handler)
        self.logger.setLevel(20)
        self.db = TinyDB('db.json')
        self.env = Environment(loader=PackageLoader('tiqueapp', 'templates'))

    def date_handler(self, obj):
        return obj.isoformat() if hasattr(obj, 'isoformat') else obj

    def parse(self, arg):
        'Convert a series of zero or more numbers to an argument tuple'
        return tuple(map(str, arg.split()))
