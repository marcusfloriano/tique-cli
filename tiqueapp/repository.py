# -*- coding: utf-8 -*-

import colorlog
import hashlib
import json

from termcolor import colored
from tinydb import TinyDB, Query
from terminaltables import AsciiTable

from tiqueapp.base import Base
from tiqueapp.client_factory import ClientFactory

class Repository(Base):

    intro = colored('Repository. Type help or ? to list commands.\n', 'green')
    prompt = colored('(tique)', 'green') + colored('[repository] ', 'yellow')

    def __init__(self):
        Base.__init__(self, 'Repository');
        self.Query = Query()

    def do_quit(self, arg):
        'End the Repository Shell'
        return True

    def dispatcher(self, arguments):
        if(arguments['new'] and arguments['<vendor>'] == 'bitbucket'):
            self.new(arguments)
        elif(arguments['list']):
            self.list()
        elif(arguments['remove']):
            self.remove(arguments['<key>'])
        elif(arguments['test']):
            self.test(arguments['<key>'])
        elif(arguments['commits'] and arguments['<tag-start>'] and arguments['<tag-end>']):
            self.commits_by_tag(arguments['<key>'],arguments['<tag-start>'],arguments['<tag-end>'])
        else:
            self.logger.error("Sorry, but the parameters is not correct. Use tique.py --version.")

    def do_new(self, args):
        'Create a new repository for Tique'
        args = self.parse(args)

        print(colored("Please select the vendor type: bitbucket"))
        vendor = raw_input("vendor: ")
        username = raw_input("username: ")
        password = raw_input("password: ")
        email = raw_input("email: ")
        owner = raw_input("owner: ")
        repository = raw_input("repository: ")

        key = repository
        key = hashlib.md5(key.encode('utf-8')).hexdigest()

        items = self.db.search((self.Query.type == 'repository') & (self.Query.key == key))
        if len(items) > 0:
            print(colored("Sorry the repository exists!", "red"))
        else:
            self.db.insert({
                'type': 'repository',
                'key' : key,
                'vendor' : vendor,
                'username' : username,
                'password' : password,
                'email' : email,
                'owner' : owner,
                'repository' : repository
            })
            print(colored('Created successfully a new repository: ' + owner + '/' + repository, "red"))

    def do_list(self,args):
        'List all repositories registred!'
        items = self.db.search((self.Query.type == 'repository'))
        table_data = [
            ['Key','Vendor','Username','Password','Email','Owner','Repository']
        ]
        for item in items:
            table_data.append([
                item['key'],
                item['vendor'],
                item['username'],
                item['password'],
                item['email'],
                item['owner'],
                item['repository']
            ])
        table = AsciiTable(table_data)
        print table.table

    def do_remove(self, args):
        args = self.parse(args)

        items = self.db.search((self.Query.type == 'repository') & (self.Query.key == key))
        if len(items) == 1:
            self.db.remove(eids=[items[0].eid])
            self.logger.warning('Removed successfully with key: ' + key)
        else:
            self.logger.error('Sorry, not exists key for remove!')

    def test(self, key):
        items = self.db.search((self.Query.type == 'repository') & (self.Query.key == key))
        if len(items) == 1:
            client = ClientFactory.instance(items[0])
            tags = client.tags()
            print json.dumps(tags, sort_keys=True, indent=4, ensure_ascii=False)
            self.logger.warning('Connect successfully with key: ' + key)
        else:
            self.logger.error('Sorry, not exists key for remove!')

    def commits_by_tag(self, key, tag1, tag2):
        items = self.db.search((self.Query.type == 'repository') & (self.Query.key == key))
        if len(items) == 1:
            client = ClientFactory.instance(items[0])
            commits = client.commits_between_tags(tag1, tag2)
            print json.dumps(commits, sort_keys=True, indent=4, ensure_ascii=False, default=self.date_handler)
        else:
            self.logger.error('Sorry, not exists key for remove!')
