# -*- coding: utf-8 -*-

from colorama import init
from termcolor import colored
import colorlog
handler = colorlog.StreamHandler()
handler.setFormatter(colorlog.ColoredFormatter('%(log_color)s%(name)s - %(message)s'))
logger = colorlog.getLogger("invoice command")
logger.addHandler(handler)
logger.setLevel(20)

from terminaltables import AsciiTable
from jinja2 import Environment, PackageLoader
env = Environment(loader=PackageLoader('tiqueapp', 'templates'))

from tiqueapp.dao.repository_dao import RepositoryDAO
from tiqueapp.dao.invoice_dao import InvoiceDAO

import click

@click.group()
def invoice():
    pass

@invoice.command()
def invoice_list():
    """ List all invoices registred in Tique! """
    invoices = InvoiceDAO().find_all()
    table_data = [
        ['Key','Name']
    ]
    for invoice in invoices:
        table_data.append([invoice['key'], invoice['name']])
    table = AsciiTable(table_data)
    print table.table

@invoice.command()
@click.argument('key')
def invoice_delete(key):
    """ Remove invoice from KEY """
    if InvoiceDAO().delete(key):
        logger.info('The invoice is deleted: ' + key)
    else:
        logger.error('Sorry, not possible remove the invoice: ' + key)

@invoice.command()
@click.argument('name')
def invoice_new(name):
    """ Add new invoice """
    saved = InvoiceDAO().new({
        'name' : name
    })
    if saved:
        print(colored('The invoice saved with success', 'green'))
    else:
        print(colored('The invoice not saved!', 'red'))

@invoice.command()
@click.argument('key')
@click.argument('repository_key')
@click.argument('tag1')
@click.argument('tag2')
def invoice_add_commits(key, repository_key, tag1, tag2):
    """ Add commits between tags to invoice """
    InvoiceDAO().add_commits(key, repository_key, tag1, tag2)

@invoice.command()
@click.argument('key')
def invoice_commits(key):
    'View the commits in invoice'
    invoice = InvoiceDAO().find_by_key(key)
    if invoice == None or not 'commits' in invoice:
        logger.error("Sorry, the invoice or commits not exists")
    else:
        table_data = [
            ['Key','Tag','Date','Message']
        ]
        for commit in invoice['commits']:
            table_data.append([
                commit['hash'],
                commit['date'],
                commit['tag'],
                commit['message']
            ])
        table = AsciiTable(table_data)
        print table.table

@invoice.command()
@click.argument('key')
@click.argument('worktime_key')
def invoice_add_worktime(key, worktime_key):
    'Add Worktime in invoice by Commits'
    InvoiceDAO().add_worktime(key, worktime_key)

@invoice.command()
@click.argument('key')
def invoice_worktimes(key):
    'View the worktimes in invoice'
    invoice = InvoiceDAO().find_by_key(key)
    if invoice == None or not 'worktimes' in invoice:
        logger.error("Sorry, the invoice or worktime not exists")
    else:
        table_data = [
            ['Date','Seconds']
        ]
        for commit in invoice['worktimes']:
            table_data.append([
                commit['date'],
                commit['seconds']
            ])
        table = AsciiTable(table_data)
        print table.table

@invoice.command()
@click.argument('key')
def invoice_ascii(key):
    'Print invoice ASCII format'
    template = env.get_template('invoice.txt')
    result = template.render(InvoiceDAO().to_ascii(key))
    result = result.replace('&#39;',"'")
    result = result.replace('&#34;','"')
    print result
