# -*- coding: utf-8 -*-

from terminaltables import AsciiTable

from tiqueapp.dao.repository_dao import RepositoryDAO

import click

@click.group()
def repository():
    pass

@repository.command()
def repository_list():
    """ List repository registred in Tique! """
    items = RepositoryDAO().find_all();
    table_data = [
        ['Key','Vendor','Username','Password','Email','Owner','Repository']
    ]
    for item in items:
        table_data.append([
            item['key'],
            item['vendor'],
            item['username'],
            "********",
            item['email'],
            item['owner'],
            item['repository']
        ])
    table = AsciiTable(table_data)
    print table.table

@repository.command()
@click.argument('key')
@click.argument('tag1')
@click.argument('tag2')
def repository_commits_by_tags(key, tag1, tag2):
    """ List commits between two tags"""
    items = RepositoryDAO().commits_between_tags(key, tag1, tag2)
    table_data = [
        ['Key', 'Tag','Date','Message']
    ]
    for item in items:
        table_data.append([
            item['hash'],
            item['tag'],
            item['date'],
            item['message']
        ])
    table = AsciiTable(table_data)
    print table.table
