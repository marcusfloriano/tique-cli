# -*- coding: utf-8 -*-
from colorama import init
from termcolor import colored
import colorlog
handler = colorlog.StreamHandler()
handler.setFormatter(colorlog.ColoredFormatter('%(log_color)s%(name)s - %(message)s'))
logger = colorlog.getLogger("invoice command")
logger.addHandler(handler)
logger.setLevel(20)

from terminaltables import AsciiTable

from tiqueapp.dao.repository_dao import RepositoryDAO
from tiqueapp.dao.worktime_dao import WorktimeDAO

import click

@click.group()
def worktime():
    pass

@worktime.command()
def worktime_list():
    """ List worktime registred in Tique! """
    items = WorktimeDAO().find_all();
    table_data = [
        ['Key','Vendor','APIKEY']
    ]
    for item in items:
        table_data.append([
            item['key'],
            item['vendor'],
            item['apikey']
        ])
    table = AsciiTable(table_data)
    print table.table

@worktime.command()
@click.argument('vendor')
@click.argument('apikey')
def worktime_new(vendor, apikey):
    """ Add new worktime vendor """
    saved = WorktimeDAO().new({
        'vendor' : vendor,
        'apikey' : apikey
    })
    if saved:
        logger.info('The worktime saved with success: ' + apikey)
    else:
        logger.error('The worktime not saved: ' + apikey)

@worktime.command()
@click.argument('key')
@click.argument('date')
def worktime_get_sec_by_date(key, date):
    """ Return seconds worktime from one date"""
    seconds = WorktimeDAO().get_seconds_by_date(key, date)
    table_data = [
        ['seconds'],
        [seconds]
    ]
    table = AsciiTable(table_data)
    print table.table
