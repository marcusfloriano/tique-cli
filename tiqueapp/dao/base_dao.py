# -*- coding: utf-8 -*-

import colorlog
from tinydb import TinyDB, Query

class BaseDAO(object):

    def __init__(self, loggername):
        handler = colorlog.StreamHandler()
        handler.setFormatter(colorlog.ColoredFormatter('%(log_color)s%(name)s - %(message)s'))
        self.logger = colorlog.getLogger(loggername)
        self.logger.addHandler(handler)
        self.logger.setLevel(20)
        self.db = TinyDB('db.json')

    def date_handler(self, obj):
        return obj.isoformat() if hasattr(obj, 'isoformat') else obj
