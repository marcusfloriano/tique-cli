
# -*- coding: utf-8 -*-

import colorlog
import hashlib
import json
from tinydb import TinyDB, Query
from tiqueapp.dao.base_dao import BaseDAO
from tiqueapp.integration.worktime_factory import WorktimeFactory

class WorktimeDAO(BaseDAO):

    def __init__(self):
        BaseDAO.__init__(self, 'WorktimeDAO');
        self.Query = Query()

    def new(self, arguments):
        key = arguments['apikey'];
        key = hashlib.md5(key.encode('utf-8')).hexdigest()
        items = self.db.search((self.Query.type == 'worktime') & (self.Query.key == key))
        if len(items) > 0:
            return False
        else:
            self.db.insert({
                'type': 'worktime',
                'key' : key,
                'vendor' : arguments['vendor'],
                'apikey' : arguments['apikey']
            })
            return True

    def remove(self, key):
        items = self.db.search((self.Query.type == 'worktime') & (self.Query.key == key))
        if len(items) == 1:
            self.db.remove(eids=[items[0].eid])
            return True
        else:
            return False

    def find_all(self):
        items = self.db.search((self.Query.type == 'worktime'))
        return items

    def get_seconds_by_date(self, key, date):
        items = self.db.search((self.Query.type == 'worktime') & (self.Query.key == key))
        if len(items) == 0:
            self.logger.error('Sorry, but the worktime not exists!')
            return None
        else:
            worktime = WorktimeFactory.instance(items[0])
            return worktime.get_seconds_by_date(date)
