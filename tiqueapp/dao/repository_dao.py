
# -*- coding: utf-8 -*-

import colorlog
import hashlib
import json
from tinydb import TinyDB, Query
from tiqueapp.dao.base_dao import BaseDAO
from tiqueapp.client_factory import ClientFactory

class RepositoryDAO(BaseDAO):

    def __init__(self):
        BaseDAO.__init__(self, 'RepositoryDAO');
        self.Query = Query()

    def new(self,data):
        pass

    def find_all(self):
        items = self.db.search((self.Query.type == 'repository'))
        if len(items) == 0:
            return None
        else:
            return items

    def commits_between_tags(self, key, tag1, tag2):
        items = self.db.search((self.Query.type == 'repository') & (self.Query.key == key))
        if len(items) == 1:
            try:
                client = ClientFactory.instance(items[0])
                commits = client.commits_between_tags(tag1, tag2)
                return commits
            except Exception, e:
                self.logger.error(e)
                self.logger.error('Sorry, error in capture the commits!')
                return None
        else:
            self.logger.error('Sorry, not exists key for remove!')
            return None
