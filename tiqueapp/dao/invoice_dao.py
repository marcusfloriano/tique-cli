
# -*- coding: utf-8 -*-

import colorlog
import hashlib
import json

import locale
locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')

import datetime
from datetime import timedelta, date

from tinydb import TinyDB, Query
from terminaltables import AsciiTable

from tiqueapp.dao.base_dao import BaseDAO
from tiqueapp.dao.worktime_dao import WorktimeDAO
from tiqueapp.dao.repository_dao import RepositoryDAO

from tiqueapp.client_factory import ClientFactory

class InvoiceDAO(BaseDAO):

    def __init__(self):
        BaseDAO.__init__(self, 'InvoiceyDAO');
        self.Query = Query()

    def new(self,args):
        key = hashlib.md5(args['name'].encode('utf-8')).hexdigest()
        invoices = self.db.search((self.Query.type == 'invoice') & (self.Query.key == key))
        if len(invoices) > 0:
            return False
        else:
            self.db.insert({'type': 'invoice', 'name': args['name'], 'key' : key})
            return True

    def delete(self, key):
        invoices = self.db.search((self.Query.type == 'invoice') & (self.Query.key == key))
        if len(invoices) == 1:
            self.db.remove(eids=[invoices[0].eid])
            return True
        else:
            return False

    def update(self, key, data):
        try:
            self.db.update(data, (self.Query.type == 'invoice') & (self.Query.key == key))
            return True
        except Exception, e:
            self.logger.error(e)
            return False


    def find_all(self):
        return self.db.search((self.Query.type == 'invoice'))

    def find_by_key(self, key):
        invoices = self.db.search((self.Query.type == 'invoice') & (self.Query.key == key))
        if len(invoices) == 0:
            return None
        else:
            return invoices[0]

    def add_commits(self, key, repository_key, tag1, tag2):
        invoice = InvoiceDAO().find_by_key(key)
        if invoice == None:
            self.logger.error('Sorry, not exists the invoce for this key!')
        else:
            commits = RepositoryDAO().commits_between_tags(repository_key, tag1, tag2)
            if commits!=None and InvoiceDAO().update(key, {'commits': commits}):
                self.logger.info('Commits add on invoce: ' + key)
            else:
                self.logger.error('Sorry, no add commits to invoice: ' + key)

    def add_worktime(self, key, worktime_key):
        invoices = self.db.search((self.Query.type == 'invoice') & (self.Query.key == key))

        if len(invoices) == 0:
            self.logger.error('Sorry, not exists the invoce!')
            return

        if len(invoices[0]['commits']) == 0:
            self.logger.error('Sorry, not exists commits for invoce!')
            return

        dates_commits = []
        for commit in invoices[0]['commits']:
            if commit['date'] not in dates_commits:
                dates_commits.append(commit['date'])

        worktimes = []
        for date in dates_commits:
            d = datetime.datetime.strptime(date, '%d/%m/%Y')
            self.logger.info('Process %s!' % d)
            worktimes.append({
                'date' : date,
                'seconds' : WorktimeDAO().get_seconds_by_date(worktime_key, d.strftime('%Y-%m-%d'))
            })

        self.db.update({'worktimes': worktimes}, (self.Query.type == 'invoice') & (self.Query.key == key))
        self.logger.warning('Worktimes add on invoce: ' + key)

    def to_ascii(self, key):
        invoices = self.db.search((self.Query.type == 'invoice') & (self.Query.key == key))
        if len(invoices) == 0:
            self.logger.error('Sorry, not exists the invoce!')
            return

        table_data = [
            ['Data','Descrição','Total de Horas','Valor']
        ]
        price_sum = 0
        for worktime in invoices[0]['worktimes']:

            if worktime['seconds'] == 0:
                continue

            message = ""
            for commit in invoices[0]['commits']:
                if commit['date'] == worktime['date']:
                    message += commit['message'];

            m, s = divmod(worktime['seconds'], 60)
            h, m = divmod(m, 60)

            price = worktime['seconds'] * (float(90) / float(3600))
            price_sum += price

            table_data.append([
                worktime['date'],
                message,
                "%d:%02d:%02d" % (h, m, s),
                locale.currency( price, grouping=True )
            ])

        table = AsciiTable(table_data)

        invoice_info = {
            'invoice_id' : key[0:7],
            'invoice_date' : '30/01/2016',
            'invoice_description' : invoices[0]['name'],
            'invoice_worktimes' : table.table,
            'invoice_total' : locale.currency( price_sum, grouping=True )
        }

        return invoice_info
