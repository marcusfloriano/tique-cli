# -*- coding: utf-8 -*-
import os
import cmd, sys
import colorlog
import hashlib
import json

import locale
locale.setlocale(locale.LC_ALL, '')

import datetime
from datetime import timedelta, date

from terminaltables import AsciiTable

from colorama import init
from termcolor import colored

from tinydb import TinyDB, Query

from docxtpl import DocxTemplate

from tiqueapp.base import Base
from tiqueapp.dao.repository_dao import RepositoryDAO
from tiqueapp.dao.invoice_dao import InvoiceDAO
from tiqueapp.dao.worktime_dao import WorktimeDAO

class Invoice(Base):

    intro = colored('Invoices.   Type help or ? to list commands.\n', 'green')
    prompt = colored('(tique)', 'green') + colored('[Invoices] ', 'yellow')

    def __init__(self):
        Base.__init__(self, 'Invoice');
        self.InvoiceQuery = Query()

    def dispatcher(self, arguments):
        if(arguments['new']):
            self.new(arguments['<name>'])
        elif(arguments['list']):
            self.list()
        elif(arguments['remove']):
            self.delete(arguments['<key>'])
        elif(arguments['add'] and arguments['commits']):
            self.add_commits(arguments['<key>'], arguments['<repository-key>'], arguments['<tag-start>'], arguments['<tag-end>'])
        elif(arguments['add'] and arguments['worktime']):
            self.add_worktime(arguments['<key>'], arguments['<worktime-key>'])
        elif(arguments['generate']):
            self.generate(arguments['<key>'])
        else:
            self.logger.error("Sorry, but the parameters is not correct. Use tique.py --version.")

    def do_generate(self, args):
        'Generate invoice from template.docx'
        args = self.parse(args)
        key = args[0]
        invoices = self.db.search((self.InvoiceQuery.type == 'invoice') & (self.InvoiceQuery.key == key))

        if len(invoices) == 0:
            self.logger.error('Sorry, not exists the invoce!')
            return


        table_data = [
            ['Data','Descrição','Total de Horas','Valor']
        ]
        price_sum = 0
        for worktime in invoices[0]['worktimes']:

            if worktime['seconds'] == 0:
                continue

            message = ""
            for commit in invoices[0]['commits']:
                if commit['date'] == worktime['date']:
                    message += commit['message'];

            m, s = divmod(worktime['seconds'], 60)
            h, m = divmod(m, 60)

            price = worktime['seconds'] * (float(90) / float(3600))
            price_sum += price

            table_data.append([
                worktime['date'],
                message,
                "%d:%02d:%02d" % (h, m, s),
                locale.currency( price, grouping=True )
            ])

        template = self.env.get_template('invoice.txt')

        table = AsciiTable(table_data)

        if not(os.path.isdir("output")):
            os.mkdir("output")
        output = open("output/invoice_" + key[0:8] + ".txt", 'w')
        result = template.render({
            'invoice_id' : key[0:8],
            'invoice_date' : '30/01/2016',
            'invoice_description' : invoices[0]['name'],
            'invoice_worktimes' : table.table,
            'invoice_total' : locale.currency( price_sum, grouping=True )
        });
        result = result.replace('&#39;',"'")
        result = result.replace('&#34;','"')
        output.write(result)
        output.close()


        # doc = DocxTemplate("invoice_template.docx")
        # doc.render({
        #     'invoice_id' : key[0:8],
        #     'invoice_date' : '30/01/2016',
        #     'invoice_description' : invoices[0]['name'],
        #     'items' : items,
        #     'invoice_total' : locale.currency( price_sum, grouping=True )
        # })
