from setuptools import setup

setup(
    name='Tique - Invoce Generator from Version Control and Worktime Software',
    version='0.1',
    py_modules=['tiqueapp'],
    include_package_data=False,
    install_requires=[
    ],
    entry_points='''
        [console_scripts]
        tique-cli=tique:cli
    ''',
)
