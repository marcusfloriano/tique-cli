#!/usr/bin/env python
# -*- coding: utf-8 -*-

from colorama import init
from termcolor import colored

from tiqueapp.commands.repository_command import repository
from tiqueapp.commands.invoice_command import invoice
from tiqueapp.commands.worktime_command import worktime

init()

import click
import os
import sys

import locale
locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')


reload(sys)
sys.setdefaultencoding('utf8')

@click.group()
def cli():
    pass

cli = click.CommandCollection(sources=[repository, invoice, worktime])

if __name__ == '__main__':
    cli()
